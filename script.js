$(document).ready(function() {

	$("#contact_form").submit(function() {
		$.ajax({
			type: "POST",
			url: "mail.php",
			data: $(this).serialize()
		}).done(function() {
			$(this).find("input").val("");
			alert("Your message is sent");
			$("#contact_form").trigger("reset");
		});
		return false;
	});
    
    $('#print').click(function(){
        window.print();
    });
});